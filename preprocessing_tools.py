
import spacy as sp
import pickle
from multiprocessing.dummy import Pool as ThreadPool
import time
from deep_translator import GoogleTranslator
import pandas as pd


def translate(text):
    t = GoogleTranslator(target="en")
    translate_text = t.translate(text)
    return translate_text

def translate_tweets(texts,n_threads=12):
    
    pool = ThreadPool(n_threads) # Threads


    if __name__=="__main__":
        try:
            results = pool.map(translate, texts)
        except Exception as e:
                raise e 
    else: 
    
        
        try:
            results = pool.map(translate, texts)
        except Exception as e:
                raise e
    pool.close()
    pool.join()     
    return results    

def twitter(doc):  
    prep_tweet=[]
    #this snipet was taken from https://jairoandres.com/spacy-procesando-tweets/
    for token in doc:
            if (
                not token.is_punct
                and not token.is_stop
                and not token.like_url
                and not token.is_space
                and not token.pos_ == "CONJ"
            ):
                prep_tweet.append(token.lemma_.lower())
    return prep_tweet



    
def save_tokens(text, file): 
    #inputs: text to be saved
    #file to save it
    with open(file, "wb") as fp:   #Pickling
        pickle.dump(text, fp)
    return
def load_tokens(file):
    with open(file, "rb") as fp:   # Unpickling
        b = pickle.load(fp)
    return b


        
def preprocess_tweets(file_name,translate=False,file="preprocessed_total_tweets.txt",
                      stopwords=["electroshock","electroconvulsive","therapy","Electroshock",
                                 "Electroconvulsive","Therapy","ECT","ect"]):
 
    if translate:
        time1=time.time()
        print("Starting to translate tweets")
        df=pd.read_excel(file_name)
        tweets=df["Tweet"].values 
        tweets_translated=translate_tweets(tweets)
        save_tokens(tweets_translated,translate)
        print("Tweets translated and saved at file %s, time needed %f"%(translate,time.time().time1))

    else: 
        tweets_translated=load_tokens(file_name)
        
    nlp=sp.load("en_core_web_lg")
    
    print("Spacy pipeline to perform:",nlp.pipe_names)
    for w in stopwords:
        nlp.vocab[w].is_stop = True

    time1=time.time()
    pipeline=nlp.pipe(tweets_translated,n_process=6,batch_size=128)
    res=[]
    for doc in pipeline:
        res.append(twitter(doc))
    print("Time to preprocess tweets",time.time()-time1)
    save_tokens(res,file)
    print("Preprocessed tweets saven at",file)
    return  res

    
        
    
    
