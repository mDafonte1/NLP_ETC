# NLP ETC

## Name
Sentiment analysis and topic modeling of electroconvulsive therapy tweets. 

## Description
This is a project to perform a sentiment analysis on electroconvulsive therapy (ECT) tweets, as well as a machine learning classification of them.


## Usage
The pipeline created in "preprocessing_tools.py" can be used for any tweet preprocessing using only some changes. The different files can be used to understand how to perform topic modelling or machine learning classification of tweets. 


## Authors and acknowledgment
Authors: Miguel Díaz-Cacho (https://gitlab.com/mDafonte1) and Francisco Jesús Lara (https://www.github.com/franciscojesuslara). 
This project can be found in the giltab repository https://gitlab.com/mDafonte1/NLP_ETC. 

## License
This project is licensed with the CC0 1.0 Universal (CC0 1.0), making it public domain. 
